<?
class PHPRepl {
  private $braceDepth = 0;
  private $bracketDepth = 0;
  private $buffer;
  private $prompt = "> ";
  private $morePrompt = "... ";
  private $stdin;
  private $hasSemi = true;
  private $rlhist; 

  function __construct($histPrefix=null) {
    $histPrefix = $histPrefix . '.' ?? '';
    $home = getenv("HOME");
    if(!file_exists("$home/.wprepl")) { mkdir("$home/.wprepl"); }
    $this->rlhist = "$home/.wprepl/${histPrefix}PHPRepl.history";
    //$this->stdin = fopen("php://stdin","r");
    function exception_error_handler($errno, $errstr, $errfile, $errline ) {
          throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
    }
    set_error_handler("exception_error_handler");

    readline_read_history($this->rlhist);
    $auto = function ($input, $idx) {
      return array_map(
        function($var) USE($input, $idx) {
          if(empty($input)) { return NULL; }
          return substr($var, strpos($var,$input));
        },
          array_filter(
            array_keys(
              array_merge(
                get_defined_vars(),
                array_diff_key(
                  $GLOBALS,
                  get_defined_vars()
                )
              )
            ), 
            function($key) USE($input,$idx) {
              $ret = substr($key, 0, strlen($input)) == $input;
              return $ret;
            } 
          )
        );
    };
    readline_completion_function($auto);
  }
  function __destruct() {
    //fclose($this->stdin);
  }
  public function countBrackets($input) {
    $this->bracketDepth = $this->bracketDepth +
        ($this->countMeta($input,"[") -   $this->countMeta($input, "]"));
  }
  public function countBraces($input) {
    $this->braceDepth = $this->braceDepth + 
      ($this->countMeta($input,"{") - $this->countMeta($input, "}"));
  }
  public function checkSemis($input) { //very naive! 
    $this->hasSemi = $this->countMeta($input,";");
  }
  public function countMeta($input, $char) {
    if(strlen($char)>1) {
      throw new Exception("Error: $char must be one character");
    }
    $pos = 0;
    $cnt = 0;
    $checked = [];
    while( ( $pos = strpos($input, $char, $pos) ) !== false ) {
      $cnt++;
      // single quote test block
      $sqchk = 0;
      while($sqchk !== false) {
        $sqchk = strpos($input,"'", $sqchk);
        if(in_array($sqchk, $checked)) { break;}
        if($sqchk !== false && $sqchk < $pos) {
          array_push($checked,$sqchk);
          $sqchk++;
          continue;
        } else if ($sqchk > $pos) {
          $pos++;
          continue 2;
        }
      }

      // double quote test block
      $dqchk = 0;
      while($dqchk !== false) {
        $dqchk = strpos($input,"\"", $dqchk);
        if(in_array($dqchk, $checked)) { break;}
        if($dqchk !== false && $dqchk < $pos) {
          array_push($checked,$dqchk);
          $dqchk++;
          continue;
        } else if ($dqchk > $pos) {
          $pos++;
          continue 2;
        }
      }
      $pos++;
      if($pos >= strlen($input)) {
        break;
      }
      if($cnt > 2**8) {
        throw new Exception("Something horribly wrong happened! Count = $cnt");
        return $cnt;
      }
    }
    return $cnt;
  }

  public function getInput() {
    do {
      $line = readline($this->prompt());
      if($line === false) {
        return false;
        break;
      }
      if(substr($line,0,5) === "clear") {
        echo exec("clear");
        break;
      }
      if(empty(trim($line))) {
        continue;
      }
      $this->countBraces($line);
      $this->countBrackets($line);
      $this->checkSemis($line);
      $this->buffer .= $line;
    } while($this->hasMore());
  }
  public function hasMore() {
    return ($this->braceDepth > 0 || 
           $this->bracketDepth > 0) ||
           !$this->hasSemi;
  }
  public function curDepth() {
    if($this->braceDepth > 0) {
      return $this->braceDepth;
    } else if($this->bracketDepth > 0) {
      return $this->bracketDepth;
    } else {
      return 0;
    }
  }
  public function prompt() {
    if($this->hasMore()) {
      return implode(array_fill(0,$this->curDepth(), $this->morePrompt));
    } else {
      return $this->prompt;
    }
  }
  public function run() {
    while($this->getInput() !== false) { 
      $ret = "";
      ob_start();
      try {
        $ret = eval($this->buffer);
      } catch (Throwable $e) {
        echo "Error: " . $e->getMessage() . PHP_EOL;
      } finally {
        $output = ob_get_clean();
        if(!empty($ret)) { echo $ret . PHP_EOL; }
        if(!empty($output)) { 
          $lines = explode("\n", $output);
          //if(sizeof($lines) > 10) {
          //  ini_set('cli.pager', 'less');
          //}
          echo $output . PHP_EOL; 
        }
        readline_add_history($this->buffer);
        $this->buffer = "";
            //ini_set('cli.pager', '');
      }
    }
    readline_write_history($this->rlhist);
  }
}
?>
