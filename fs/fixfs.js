#!/usr/bin/env node
'use strict';
const fs = require('fs');
const readline = require('readline');
const AwaitLock = require('await-lock').default;
const {cpanel} = require('./cp.js');
const crypto = require('crypto');

const lock = new AwaitLock();
const fsPostfix = '/plugins/fs-poster';
const mailpoet = '/plugins/mailpoet';
const subs = '/lib/Util/License/Features/Subscribers.php';
const fbLib = '/App/Lib/fb/FacebookApi.php';
const liLib = '/App/Lib/linkedin/Linkedin.php';
const modal = '/App/view/modals/schedule/plan_post.php';
const fcns = '/themes/Divi-Child/functions.php';
const uito = '/themes/Divi-Child/UitoPub.class.php';
const amp = '/themes/Divi-Child/amp-page.php';
const wpContent= '/wp-content';
let wp; let fsPath; let fcnPath; let uitoPath;
let mpPath; let subsPath;
let ampPath; let home;

async function run() {
  const args = process.argv.slice(2);
  if (!args.length) {
    console.error('I need a username to check!');
    return 1;
  } else {
    const usr = args.shift().replace(/\/+$/, '');
    const user = await cpanel.getUser(usr);
    if (user === false) {
      console.error('User not found:', usr);
      return 1;
    }
    console.log('User found:', usr, '-', home);
    const steps = {
      regular: [
        [fixModal, [usr]],
        [fixPoster, [usr, usr, 'facebook']],
        [fixPoster, [usr, usr, 'linkedin']],
        [fixMailPoet, [usr]],
        [injectScripts, [usr]],
      ],
      guardian: [
        [injectScript, [usr, 'ghostfix']],
      ],
    };
    for (const domain of user.domains) {
      home = domain.dir;
      console.log('Processing', domain.domain);
      try {
        wp = home + wpContent;
        fs.accessSync(wp, fs.F_OK);
        fsPath = wp + fsPostfix;
        fcnPath = wp + fcns;
        uitoPath = wp + uito;
        ampPath = wp + amp;
        mpPath = wp + mailpoet;
        subsPath = mpPath + subs;
        // const targetPage = args.shift();
        const parts = domain.domain.split(/\./);
        if (parts.length == 3 && parts[0] === 'guardian-designs') {
          process = steps.guardian;
        } else {
          process = steps.regular;
        }
        for (const step of process) {
          const [func, args] = step;
          try {
            console.log(await func.apply(null, args));
          } catch (e) {
            console.log(`error in ${func.name}: ${e}`);
          }
        }
      } catch (e) {
        console.log(domain.domain+': not a wordpress installation. skipping');
      }
    }
  }
}

async function fixModal(usr) {
  return new Promise((resolve, reject) => {
    console.log(`Modifying plan_post.php for ${usr}`);
    const check = fs.readFileSync(
        `${fsPath}${modal}`,
    ).toString().match(/FSFIX/);
    if (!check) {
      const res = [];
      const rl = readline.createInterface({
        input: fs.createReadStream(`${fsPath}${modal}`),
      });
      let i = 0;
      rl.on('line', (line) => {
        if (line.match(
            /change_(custom_messages|accounts)_btn".\.length == 0/,
        )) {
          res.push(
              '// FSFIX - BEGIN sharefix block' + (++i),
              line.replace(/length == 0/, 'length != null'),
              '// FSFIX - END sharefix block' + i,
          );
        } else if (line.match(/setTimeout/)) {
          res.push(
              '// FSFIX - BEGIN noredir block',
              '/*'+line,
          );
        } else if (line.match(/, 1000/)) {
          res.push(
              line+'*/',
              '// FSFIX - END noredir block',
          );
        } else {
          res.push(line);
        }
      });
      rl.on('close', () => {
        fs.writeFile(`${fsPath}/${modal}`, res.join('\n'), (err) => {
          if (err) {
            reject(new Error(`Error writing to ${fsPath}${modal}: ${err}`));
          } else {
            resolve(`Wrote changes to ${fsPath}${modal}`);
          }
        });
      });
    } else {
      resolve('plan_post.php already contains FSFIX block. skipping...');
    }
  });
}

async function fixMailPoet(usr) {
  return new Promise((resolve, reject) => {
    console.log(`Modifying Subscribers.php for ${usr}`);
    let wrote = false;
    try {
      fs.accessSync(subsPath, fs.F_OK);
    } catch (e) {
      resolve('Subscribers.php not found. Skipping.');
    }
    const check = fs.readFileSync(subsPath).toString().match(/FSFIX/);
    if (!check) {
      const res = [];
      const rl = readline.createInterface({
        input: fs.createReadStream(subsPath),
      });
      const i = 0;
      rl.on('line', (line) => {
        res.push(line);
        if (line.match(
            /function getMssSubscribersLimit/i,
        )) {
          res.push(
              '// FSFIX - BEGIN mailpoet.subscribers block',
              'return PHP_INT_MAX;',
              '// FSFIX - END mailpoet.subscribers block',
          );
          wrote = true;
        } /* else {
          res.push(line);
        } */
      });
      rl.on('close', () => {
        if(wrote) {  
        fs.writeFile(subsPath, res.join('\n'), (err) => {
          if (err) {
            reject(new Error(`Error writing to ${subsPath}: ${err}`));
          } else {
            resolve(`Wrote changes to ${subsPath}`);
          }
        });
        } else {
          resolve(`MssSubscribersLimit not found in ${subsPath}`);
        } 
      });
    } else {
      resolve('Subscribers.php already contains FSFIX block. skipping...');
    }
  });
}

async function fixPoster(usr, targetPage, network) {
  return new Promise((resolve, reject) => {
    console.log(`Modifying ${network} for ${usr}...`);
    network = network.toLowerCase();
    network = network == 'linkedin' ? network : 'facebook';
    const lib = network == 'linkedin' ? liLib : fbLib;
    const check = fs.readFileSync(`${fsPath}${lib}`).
        toString().match(/FSFIX - BEGIN UitoPub/);
    if (!check) {
      const res = [];
      const rl = readline.createInterface({
        input: fs.createReadStream(`${fsPath}${lib}`),
      });
      targetPage = typeof targetPage === 'undefined' ?
                    usr : targetPage;
      rl.on('line', (line) => {
        if (line.match(/return \$result2;/)) {
          res.push(
              '// FSFIX - BEGIN UitoPub block',
              '',
              '$id = url_to_postid($link);',
              'if($id !== 0) { ',
              '  $post = get_post($id);',
              '  $accept = ["tribe_events"];',
              '  if(in_array(',
              '    $post->post_type,',
              '    $accept',
              '  ) && $result2["status"] == "ok") {  ',
              '    include_once("'+uitoPath+'");',
              '    $uitopost = new \\Guardian\\UitoPub("'+targetPage+'");',
              '    $uitopost->'+network+'($result2["id"]);',
              '  }',
              '}',
              '',
              '// FSFIX - END UitoPub block',
          );
        }
        res.push(line);
      });
      rl.on('close', () => {
        fs.writeFile(`${fsPath}/${lib}`, res.join('\n'), (err) => {
          if (err) {
            reject(new Error(`Error writing to ${fsPath}${lib}: ${err}`));
          } else {
            resolve(`Wrote changes to ${fsPath}${lib}`);
          }
        });
      });
    } else {
      resolve(
          lib+' already contains the FSFIX block, skipping...',
      );
    }
  });
}

async function injectScripts(usr) {
  return new Promise(async (resolve, reject) => {
    const out = [];
    const files = fs.readdirSync(__dirname+'/php');
    for (const file of files) {
      if (file === 'cp') {
        continue;
      }
      const script = file.replace(/\.php/, '');
      try {
        out.push(await injectScript(usr, script));
      } catch (e) {
        out.push('Error injecting '+script, e);
      }
    }
    resolve('It is done.'); // out.join('\n'));
  });
}
async function injectScript(usr, script) {
  return new Promise(async (resolve, reject) => {
    console.log(`Injecting ${script} into theme for ${usr}...`);
    const path = __dirname+`/php/${script}.php`;
    let fix = fs.readFileSync(path).toString();
    fix = fix.replace(/(<\?(php)?|\?>)\n?/ig, '').trim();
    const injects = {
      uitopub: {
        from: 'cp/UitoPub.class.php',
        to: uitoPath,
      },
      amp: {
        from: 'cp/amp-page.php',
        to: ampPath,
      },
    };
    if (injects[script]) {
      const from = injects[script].from;
      const to = injects[script].to;
      try {
        fs.accessSync(to);
        console.log(`${to} already exists, skipping`);
      } catch (E) {
        const content = fs.readFileSync(__dirname+'/php/'+from);
        await lock.acquireAsync();
        fs.writeFile(to, content, (e) => {
          if (e) {
            lock.release();
            reject(new Error(`Error writing ${to}: ${e}`));
          } else {
            console.log(`${to} created.`);
            lock.release();
          }
        });
      }
    }
    try {
      fs.accessSync(fcnPath, fs.F_OK);
    } catch (e) {
      return reject(new Error(fcnPath + ' not found, error: '+e));
    }
    const content = fs.readFileSync(fcnPath).
        toString();
    const code = content.match(
        new RegExp('// FSFIX - BEGIN '+script+'\s*([^]+?)\n+// FSFIX - END '+script, 'im'),
    );
    let omit = false;
    let omitting = false;
    let codeExists = false;
    if (code) {
      console.log(script,
          'block already detected. checking if code differs...',
      );
      const oldCode = code[1].trim();
      const fixSum = sha256(fix);
      const oldSum = sha256(oldCode);
      if (fixSum === oldSum) {
        codeExists = true;
      } else {
        omit = true;
        console.log('Code differs. Replacing block.');
      }
    }
    if (!codeExists) {
      console.log(`Writing ${script} section to ${fcnPath}...`);
      const res = [];
      const rl = readline.createInterface({
        input: fs.createReadStream(fcnPath),
      });
      const fsBeg = new RegExp('// FSFIX - BEGIN '+script);
      const fsEnd = new RegExp('// FSFIX - END '+script);
      rl.on('line', (line) => {
        if (line.match(fsBeg) && omit) {
          omitting = true;
          return;
        }
        if (line.match(fsEnd) && omit) {
          omitting = false;
          return;
        }
        if (!omitting) {
          res.push(line);
        }
      });
      rl.on('close', async () => {
        const last = res.pop();
        res.push(
            '// FSFIX - BEGIN ' + script,
            fix,
            '// FSFIX - END ' + script,
            '',
        );
        res.push(last);
        await lock.acquireAsync();
        fs.writeFile(fcnPath, res.join('\n'), (err) => {
          if (err) {
            reject(new Error(`Error writing to ${fcnPath}: ${err}`));
          } else {
            resolve(`Wrote ${script} section to ${fcnPath}`);
          }
          lock.release();
        });
      });
    } else {
      resolve(
          console.log('functions.php already contains the '+script+' block, skipping...'),
      );
    }
  });
}

/* async function injectFSFixer(usr) {
  return new Promise((resolve, reject) => {
    console.log(`Injecting FSFixer into functions.php for ${usr}...`);
    let fix = fs.readFileSync(__dirname+'/php/chkupg.php').toString().trim();
    const check = fs.readFileSync(fcnPath).
        toString().match(/FSFIX - BEGIN chkupg/);
    if (!check) {
      console.log(`Writing chkupg section to ${fcnPath}...`);
      const res = [];
      const rl = readline.createInterface({
        input: fs.createReadStream(fcnPath),
      });
      rl.on('line', (line) => {
        res.push(line);
      });
      rl.on('close', async () => {
        const last = res.pop();
        res.push(
            '// FSFIX - BEGIN chkupg block',
            ' ',
            fix,
            '// FSFIX - END chkupg block',
            ' ',
        );
        res.push(last);
        await lock.acquireAsync();
        fs.writeFile(fcnPath, res.join('\n'), (err) => {
          if (err) {
            reject(new Error(`Error writing chkupg to ${fcnPath}: ${err}`));
          } else {
            resolve(`Wrote chkupg block to ${fcnPath}`);
          }
          lock.release();
        });
      });
    } else {
      resolve(
          'functions.php already contains the chkupg block, skipping...',
      );
    }
  });
}

async function injectGhostfix(usr) {
  return new Promise((resolve, reject) => {
    console.log(`Injecting Divi Ghoster fix into functions.php for ${usr}...`);
    let fix = fs.readFileSync(__dirname+'/php/ghostfix.php').toString();
    const check = fs.readFileSync(fcnPath).
          toString().match(/FSFIX - BEGIN ghostfix/i);
    if (!check) {
      console.log(`Writing ghostfix section to ${fcnPath}...`);
      const res = [];
      const rl = readline.createInterface({
        input: fs.createReadStream(fcnPath),
      });
      rl.on('line', (line) => {
        res.push(line);
      });
      rl.on('close', async () => {
        const last = res.pop();
        res.push(
            '// FSFIX - BEGIN ghostfix block',
            ' ',
            fix,
            '// FSFIX - END ghostfix block',
            ' ',
        );
        res.push(last);
        await lock.acquireAsync();
        fs.writeFile(fcnPath, res.join('\n'), (err) => {
          if (err) {
            reject(new Error(`Error writing ghostfix to ${fcnPath}: ${err}`));
          } else {
            resolve(`Wrote ghostfix block to ${fcnPath}`);
          }
          lock.release();
        });
      });
    } else {
      resolve(
          'functions.php already contains the ghostfix block, skipping...',
      );
    }
  });
} */

function sha256(text) {
  return crypto.createHash('sha256').update(text).digest('hex');
}

(async () => {
  await run();
})();
