#!/usr/bin/env node
const cpanel = require('cpanel-lib');
const {options} = require('./whm.auth');

class cPaneler {

  constructor() {
    this.client = cpanel.createClient(options);
  }

  getUsers() {
    return new Promise(async (resolve, reject) => {
      this.client.call('listaccts', {}, async (err, res) => {
        if(err) {
          reject(err);
        } else {
          const accounts = res.acct.map((e) => {
            return {
              user: e.user,
              domains: [
                {domain: e.domain, dir: `/home/${e.user}/public_html`}
              ]
            };
          });
          for(const idx in accounts) {
            const subs = await this.getDomains(accounts[idx].user);
            subs.forEach(sub => accounts[idx].domains.push(sub));
          }
          resolve(accounts);
        }
      })
    })
  }

  async getUser(user) {
    return new Promise(async (resolve, reject) => {
      this.client.call('listaccts', {searchtype: 'user', search: user}, async (err, res) => {
        if(err) {
          reject(err);
        } else {
          if(!res.acct.length) { return resolve(false); }
          const account = res.acct.shift();
          const ret = {
            user: account.user,
            domains: [
              {domain: account.domain, dir: `/home/${account.user}/public_html`}
            ]
          };
          const subs = await this.getDomains(ret.user);
          subs.forEach(sub => ret.domains.push(sub));
          resolve(ret);
        }
      })
    })
  }
  async getDomains(user) {
    return new Promise((resolve, reject) => {
      this.client.callApi2('SubDomain', 'listsubdomains', {}, user, (err, res) => {
        if( err ) { reject(err) }
        else {
          const domains = res.cpanelresult.data;
          resolve(domains.map(e => {
            return { domain: e.domain, dir: e.dir };
          }));
        }
      })
    })
  }
}


module.exports.cpanel = new cPaneler();
