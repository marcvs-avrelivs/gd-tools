CREATE TABLE `::prefix::fs_account_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `driver` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cookies` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT
