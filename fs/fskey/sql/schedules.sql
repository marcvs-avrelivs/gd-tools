CREATE TABLE `::prefix::fs_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `interval` int(11) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insert_date` timestamp NULL DEFAULT current_timestamp(),
  `share_time` time DEFAULT NULL,
  `post_type_filter` varchar(750) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_filter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_sort` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_date_filter` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_ids` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_execute_time` timestamp NULL DEFAULT NULL,
  `custom_post_message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `share_on_accounts` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sleep_time_start` time DEFAULT NULL,
  `sleep_time_end` time DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=401 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

