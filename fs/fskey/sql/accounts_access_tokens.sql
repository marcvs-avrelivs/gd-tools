CREATE TABLE `::prefix::fs_account_access_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `expires_on` timestamp NULL DEFAULT NULL,
  `access_token` varchar(2500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token_secret` varchar(750) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refresh_token` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

