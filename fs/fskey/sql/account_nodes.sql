CREATE TABLE `::prefix::fs_account_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `node_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `node_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_date` timestamp NULL DEFAULT current_timestamp(),
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fan_count` bigint(20) DEFAULT NULL,
  `cover` varchar(750) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `screen_name` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;


