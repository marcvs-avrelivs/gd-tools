CREATE TABLE `::prefix::fs_apps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `driver` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_secret` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_key` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_authenticate_link` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_standart` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
