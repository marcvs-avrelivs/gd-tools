CREATE TABLE `::prefix::fs_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) DEFAULT NULL,
  `node_id` int(11) DEFAULT NULL,
  `node_type` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sended` tinyint(1) DEFAULT 0,
  `status` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error_msg` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_time` timestamp NULL DEFAULT current_timestamp(),
  `interval` int(11) DEFAULT NULL,
  `driver_post_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visit_count` int(11) DEFAULT 0,
  `feed_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `driver_post_id2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_post_message` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `share_on_background` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=320 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

