<?
function fsfix_append_date($id, $post, $updoot) {
 if($post->post_type === "tribe_events") {
   if($post->post_status === "auto-draft") { return; }
   $title = $post->post_title;
   $ev = get_post_meta($id, "_EventStartDate", true);
   if(!$ev) {
     //nope no event start date
     return;
   }
   $tiem = strtotime($ev);
   $date = date('m/d/y', $tiem);
   $title = dateit($title);
   $title = trim($title);
   $title .= " ($date)";
   $update = [ "ID" => $post->ID, "post_title" => $title ];
   remove_action( 'wp_insert_post', 'fsfix_append_date'); //definitely necessary to prevent infinite loop
   if(wp_update_post($update) === $post->ID) {
     // it would appear that it worked
   }
   // now i need to update the meta owo
   $meta = trim(get_post_meta($id, '_yoast_wpseo_metadesc', true)); // grasp meta
   if($meta == '') { //it is empty. i must fetch default template!
     $titles = maybe_unserialize(get_option('wpseo_titles'));
     if($titles !== false) {
       $meta = $titles['metadesc-tribe_events'];
     }
   }
   $newmeta = dateit($meta); // transmute meta
   if($newmeta == '' && $newmeta != $meta) {
     // some weird sorcery happened here. probably best to
     // just not do anything.
   } else {
     $newmeta .= " ($date)"; // append date aaaaand roll credits
     $updooter = update_post_meta($id, '_yoast_wpseo_metadesc', $newmeta); // commit da change
   }
   add_action( 'wp_insert_post', 'fsfix_append_date', 10, 3 ); //aaaaaand restore the thing just in case !
  }
}

function dateit($txt) {
     $dmatch = '/\(\d+\/\d+\/\d+\)$/';
     $matches = [];
     $match = preg_match( $dmatch, $txt, $matches );
     if($match)  { 
       $txt = preg_replace($dmatch, '', $txt); 
     }
     return $txt;
}

add_action( 'wp_insert_post', 'fsfix_append_date', 10, 3 );
?>
