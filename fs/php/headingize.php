<?
function fsfix_headingize($query) {
  $tax = array('product_cat','product_tag', 'giftcard-cat', 'product_shipping_class');
if(is_archive() && $query->is_main_query() && !is_post_type_archive(array('tribe_events','product')) &&
  !is_tax($tax) ) {
  the_archive_title( '<h1 class="page-title">', '</h1>'   );
  }
}
add_action( 'loop_start', 'fsfix_headingize'  );
?>
