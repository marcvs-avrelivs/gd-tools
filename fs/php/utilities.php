<?
function fsfix_getHome() {
  return realpath(ABSPATH . "/..");
}

function fsfix_getUsername() { 
  return basename(fsfix_getHome());
}

function fsfix_log($msg) {
  $date = date('c');
  $trace = debug_backtrace();
  $caller = '';
  $path = fsfix_getHome();
  $path .= '/fsfix.log';
  if(!empty($trace[1])) {
    $caller = $trace[1]['function'];
  }
  $line = "[" . (empty($caller) ? "" : "$caller::") . "$date] $msg\n";
  file_put_contents($path, $line, FILE_APPEND);
}
?>
