<?
function fsfix_upgchk( $upgrader_object, $options ) {
   $chk = ['fs-poster', 'mailpoet', 'mailpoet-premium']; //plugin_basename( __FILE__  );
   $doc_root = $_SERVER["DOCUMENT_ROOT"];
   $hDir = realpath("$doc_root/..");
   $userName = basename($hDir);
   $fsfix = false;
   $fskey = false;
   $plugs = [];
   // If an update has taken place and the updated type is plugins and the plugins element exists
   if( $options['action'] == 'update' && $options['type'] == 'plugin' && isset( $options['plugins']  )  ) {
     // Iterate through the plugins being updated and check if ours is there
     foreach( $options['plugins'] as $plugin  ) {
       $plugDir = dirname($plugin);
       $version = get_plugin_data(ABSPATH . "wp-content/plugins/$plugin")['Version'];
       if( in_array( $plugDir, $chk ) ) {
       // Set a transient to record that our plugin has just been updated
         $fsfix = true;
         if($plugDir == 'fs-poster') {
           $fskey = true;
         }
         set_transient( "wp_" . $plugin . "_updated", 1  );
       }
       array_push($plugs,"$plugDir -> v$version");
     }
     if($fsfix) {
       exec('/var/node/gd-tools/fs/fsfix ' . $userName);
     }
     if($fskey) {
       exec('/var/node/gd-tools/fs/fskey/fskey ' . $username);
     }
     fsfix_ping_slack("Plugins updated:\n```" . implode("\n", $plugs)."```");
   }
 }

function fsfix_ping_slack($message) {
 $url = 'https://hooks.slack.com/services/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';
 $ch = curl_init($url);
 $sUrl = get_site_url();
 $data = ["text" => "$sUrl: $message"];
 $data_string = json_encode($data);
 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
 curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json',
     'Content-Length: ' . strlen($data_string)
    )
 );
 $result = curl_exec($ch);

}
add_action( 'upgrader_process_complete', 'fsfix_upgchk', 10, 2  );
?>
