<?
function fsfix_ghostfix() {
  echo <<<"EOD"
   <style type='text/css'>
   #wpcontent {
     padding-left: 0px !important;
   }
   #wpbody {
     padding-left: 1.5% !important;
   }
   #wpbody-content {
     padding: 0px 0px 30px 0px !important;
     margin-left: 0px !important;
   }
  .modal {
    max-width: 100% !important;
  }
   </style>
EOD;
}
add_action('admin_head', 'fsfix_ghostfix');
?>
