<?
namespace Guardian;
class UitoPub {
  private $channel = "uitopost";
  private $targetPage;
  private $redis;

  function __construct($target) {
    require_once "Predis/Autoloader.php";
    \Predis\Autoloader::register();
    $this->redis = new \Predis\Client(array(
      'host' => 'localhost',
      'scheme' => 'tcp',
      'port' => 6379
    )); 
    $this->targetPage = $target;
  }

  function __destruct() {
    $this->redis->disconnect();
  }

  public function pub($message, $comment="") {
    $message['targetPage'] = $this->targetPage;
    if(!empty($comment)) {
      $message["comment"] = $comment;
    }
    $this->redis->publish($this->channel, json_encode($message));
  }

  public function facebook($id) {
    $this->pub(
      [
        'network' => 'facebook',
        'postID' => $id
      ]
    );
  }
  public function linkedin($id) {
    $this->pub(
      [
      'network' => 'linkedin',
      'postID' => $id
      ]
    );
  }
}

?>
